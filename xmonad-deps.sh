#!/bin/bash

for i in \
imlib2 \
giblib \
feh \
scrot \
dmenu \
ttf-bitstream-vera \
; do
sbopkg -B -i $i || exit 1
done
